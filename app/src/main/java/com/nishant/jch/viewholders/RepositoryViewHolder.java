package com.nishant.jch.viewholders;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.nishant.jch.R;
import com.nishant.jch.models.RepositoryItem;
import com.nishant.jch.utils.Utilities;

public class RepositoryViewHolder extends RecyclerView.ViewHolder {

    private final AppCompatImageView imageView;
    private final AppCompatTextView name;
    private final AppCompatTextView description;
    public final ConstraintLayout subContainer;
    public final View itemView;
    private final AppCompatTextView language;
    private final AppCompatTextView stars;
    private final AppCompatTextView forks;

    public RepositoryViewHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
        imageView = itemView.findViewById(R.id.imageView);
        name = itemView.findViewById(R.id.name);
        description = itemView.findViewById(R.id.description);
        subContainer = itemView.findViewById(R.id.sub_container);
        language = itemView.findViewById(R.id.language);
        stars = itemView.findViewById(R.id.stars);
        forks = itemView.findViewById(R.id.forks);
    }

    public void bindData(RepositoryItem repositoryItem, final boolean isExpanded) {
        Utilities.loadPic(repositoryItem.getAvatar(), imageView);
        name.setText(repositoryItem.getName());
        description.setText(repositoryItem.getDescription());
        subContainer.setVisibility(isExpanded?View.VISIBLE:View.GONE);
        itemView.setActivated(isExpanded);
        language.setText(repositoryItem.getLanguage());
        stars.setText(String.valueOf(repositoryItem.getStars()));
        forks.setText(String.valueOf(repositoryItem.getForks()));
    }
}
