package com.nishant.jch.network;

import com.nishant.jch.models.RepositoryItem;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface Apis {

    @GET("/repositories")
    Single<List<RepositoryItem>> fetchRepositories();
}
