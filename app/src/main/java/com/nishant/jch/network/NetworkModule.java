package com.nishant.jch.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nishant.jch.utils.Constants;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkModule {

    private Retrofit retrofit;

    private GsonConverterFactory providesGsonConverter() {
        Gson gson = new GsonBuilder().create();
        return GsonConverterFactory.create(gson);
    }

    private OkHttpClient provideOkHttpClient() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(httpLoggingInterceptor);
        return builder.build();
    }

    public synchronized Apis getApis() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.SERVER_BASE_URL)
                    .client(provideOkHttpClient())
                    .addConverterFactory(providesGsonConverter())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }

        return retrofit.create(Apis.class);
    }
}
