package com.nishant.jch.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.TransitionManager;

import com.nishant.jch.R;
import com.nishant.jch.models.RepositoryItem;
import com.nishant.jch.viewholders.RepositoryViewHolder;

import java.util.ArrayList;
import java.util.List;

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryViewHolder> {

    private List<RepositoryItem> repositoryItemList = new ArrayList<>();
    private int mExpandedPosition= -1;
    private RecyclerView recyclerView = null;

    @NonNull
    @Override
    public RepositoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RepositoryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.repositories_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RepositoryViewHolder holder, int position) {
        final boolean isExpanded = position==mExpandedPosition;
        holder.bindData(repositoryItemList.get(position), isExpanded);
        holder.itemView.setOnClickListener(v -> {
            mExpandedPosition = isExpanded?RecyclerView.NO_POSITION:position;
            TransitionManager.beginDelayedTransition(recyclerView);
            notifyDataSetChanged();
        });
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    @Override
    public int getItemCount() {
        return Math.max(repositoryItemList.size(), 0);
    }

    public void updateListItems(List<RepositoryItem> repositoryItemList) {
        this.repositoryItemList = repositoryItemList;
        notifyDataSetChanged();
    }
}
