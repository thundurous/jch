package com.nishant.jch.repositories.remote;

import com.nishant.jch.models.RepositoryItem;

import java.util.List;

import io.reactivex.Single;

public interface RepositoryContract {

    Single<List<RepositoryItem>> fetchRepositories();
}
