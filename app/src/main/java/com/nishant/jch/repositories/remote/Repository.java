package com.nishant.jch.repositories.remote;

import android.content.Context;

import com.nishant.jch.models.RepositoryItem;
import com.nishant.jch.network.Apis;
import com.nishant.jch.network.NetworkModule;
import com.nishant.jch.utils.Utilities;

import java.util.List;
import io.reactivex.Single;

public class Repository implements RepositoryContract {

    private NetworkModule networkModule;
    private Context context;

    public Repository(NetworkModule networkModule, Context context) {
        this.networkModule = networkModule;
        this.context = context;
    }

    @Override
    public Single<List<RepositoryItem>> fetchRepositories() {
        return Single.just(Utilities.loadRepositories(context));
    }
}
