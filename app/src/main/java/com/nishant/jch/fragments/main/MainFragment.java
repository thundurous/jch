package com.nishant.jch.fragments.main;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.nishant.jch.R;
import com.nishant.jch.adapters.RepositoryAdapter;
import com.nishant.jch.models.RepositoryItem;
import com.nishant.jch.utils.Utilities;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    private RepositoryAdapter repositoryAdapter;
    private RecyclerView recyclerView;
    private ShimmerFrameLayout shimmerFrameLayout;
    private ConstraintLayout error_layout;
    private AppCompatTextView retry;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<RepositoryItem> repositoryItemList;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        repositoryAdapter = new RepositoryAdapter();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        View view = inflater.inflate(R.layout.main_fragment, container, false);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
//        inflater.inflate(R.menu.menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        error_layout = view.findViewById(R.id.error_layout);
        retry  = view.findViewById(R.id.retry_textView);
        retry.setOnClickListener(v -> onRetryClicked());
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::fetchData);
        shimmerFrameLayout = view.findViewById(R.id.shimmerViewContainer);
        shimmerFrameLayout.startShimmerAnimation();
        setRepositoryAdapter(view);
        setupObserver();
        fetchData();
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.sort_by_stars:
                Collections.sort(repositoryItemList, (o1, o2) -> o1.getStars().compareTo(o2.getStars()));
                repositoryAdapter.notifyDataSetChanged();
                return true;
            case R.id.sort_by_name:
                Collections.sort(repositoryItemList, (o1, o2) -> o1.getName().compareTo(o2.getName()));
                repositoryAdapter.notifyDataSetChanged();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setRepositoryAdapter(View view) {
        recyclerView = view.findViewById(R.id.list_items);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        recyclerView.setAdapter(repositoryAdapter);
    }

    private void setupObserver() {
        mViewModel.repositoryList.observe(getViewLifecycleOwner(), this::updateRepositories);
        mViewModel.generalError.observe(getViewLifecycleOwner(), this::setError);
    }

    private void fetchData() {
        error_layout.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        shimmerFrameLayout.setVisibility(View.VISIBLE);
        shimmerFrameLayout.startShimmerAnimation();
        new Handler().postDelayed(() -> mViewModel.fetchCategories(), 2000);
    }

    private void updateRepositories(List<RepositoryItem> repositoryItemList) {
        this.repositoryItemList = repositoryItemList;
        swipeRefreshLayout.setRefreshing(false);
        shimmerFrameLayout.stopShimmerAnimation();
        shimmerFrameLayout.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        repositoryAdapter.updateListItems(repositoryItemList);
    }

    private void setError(Throwable throwable){
        swipeRefreshLayout.setRefreshing(false);
        shimmerFrameLayout.stopShimmerAnimation();
        shimmerFrameLayout.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        error_layout.setVisibility(View.VISIBLE);
    }

    private void onRetryClicked() {
        Utilities.shrinkWithEndAction(retry, 300, null, new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                Utilities.growWithEndAction(retry, 300, null, new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        fetchData();
                    }
                });
            }
        });
    }
}