package com.nishant.jch.fragments.main;

import android.annotation.SuppressLint;
import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.nishant.jch.models.RepositoryItem;
import com.nishant.jch.network.NetworkModule;
import com.nishant.jch.repositories.remote.Repository;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel extends AndroidViewModel {

    protected final MutableLiveData<List<RepositoryItem>> repositoryList = new MutableLiveData<>();
    protected final MutableLiveData<Throwable> generalError = new MutableLiveData<>();
    private final Repository repository = new Repository(new NetworkModule(), getApplication());

    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    @SuppressLint("CheckResult")
    public void fetchCategories() {
        repository.fetchRepositories().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(repositoryList::postValue, generalError::postValue);
    }
}