package com.nishant.jch.utils;

import android.animation.Animator;
import android.content.Context;
import android.content.res.AssetManager;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nishant.jch.models.RepositoryItem;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import org.json.JSONArray;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Utilities {
    public static void loadPic(String profilePic, ImageView into) {
        RequestCreator creator = Picasso.get()
                .load(profilePic)
                .fit()
                .centerCrop();

        into.post(() -> creator.into(into));
    }

    public static void shrinkWithEndAction(View view, long duration, Runnable runnable, Animator.AnimatorListener listener) {
        ViewPropertyAnimator viewPropertyAnimator = view.animate().scaleX(0.70f).scaleY(0.70f).setDuration(duration);
        if(runnable!=null)
            viewPropertyAnimator.withEndAction(runnable);
        if(listener!=null)
            viewPropertyAnimator.setListener(listener);
    }

    public static void growWithEndAction(View view, long duration, Runnable runnable, Animator.AnimatorListener listener) {
        ViewPropertyAnimator viewPropertyAnimator = view.animate().scaleX(1.0f).scaleY(1.0f).setDuration(duration);
        if(runnable!=null)
            viewPropertyAnimator.withEndAction(runnable);
        if(listener!=null)
            viewPropertyAnimator.setListener(listener);
    }

    private static String loadJSONFromAsset(Context context, String jsonFileName) {
        String json = null;
        InputStream is = null;
        try {
            AssetManager manager = context.getAssets();
            is = manager.open(jsonFileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static List<RepositoryItem> loadRepositories(Context context) {
        try {
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            JSONArray array = new JSONArray(loadJSONFromAsset(context, "repositories.json"));
            List<RepositoryItem> repositoryItemList = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {
                RepositoryItem repositoryItem = gson.fromJson(array.getString(i), RepositoryItem.class);
                repositoryItemList.add(repositoryItem);
            }
            return repositoryItemList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
